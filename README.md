# Wardrobify

Team:

* Person 1 - Alex: shoes
* Person 2 - George: hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The shoes model will include properties for manufacturer, model name, color, picture URL, and the bin in the wardrobe where it exists.  To synchronize the shoes entity with the wardrobe entity, we'd create a wardrobe value object in the shoes microservice.

## Hats microservice

Hats microservice will need to be able to pull location data from the wardrobe API. We will enable this with a LocationVO. Some other tasks will include creating the Hats model and views so that it is a fully functional CRUD app. Lastly, we will need to build react pages to list hats as well as create new ones. 