import React from 'react';
import { Link } from 'react-router-dom';
import ShoeDelete from './ShoeDelete';


class ShoeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shoes: [],
        }
    }

    async componentDidMount() {
        const resp = await fetch('http://localhost:8080/api/shoes/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                shoes: data.shoes
            })
        } else {
            console.log("There was an error fetching list of shoes: ", resp);
        }
    }


    render() {
        return (
            <React.StrictMode>
            <div>
                <br />
                <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Create New Shoe</Link>
                <br />
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Model Name</th>
                            <th>Picture</th>
                            <th>Manufacturer</th>
                            <th>Color</th>
                            <th>Bin Number</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.shoes.map(shoe => {
                        return (
                        <tr key={shoe.id}>
                            <td><Link to={`/shoes/details/${shoe.id}`} className="px-4 gap-3">{shoe.model_name}</Link></td>
                            <td><img src={shoe.picture_url} width="150"/></td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin.id}</td>
                            <td><button onClick={() => ShoeDelete(shoe.id)} className="btn btn-primary btn-lg px-4 gap-3">Delete Shoe</button></td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
                <br />
            </div>
            </React.StrictMode>
        );
    }
}    

export default ShoeList