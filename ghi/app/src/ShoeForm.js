import React from 'react';
import { Link, useNavigate } from 'react-router-dom';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modelName: "",
            pictureUrl: "",
            manufacturer: "",
            color: "",
            bins: [],
        }
    }
    
    async componentDidMount() {
        const resp = await fetch('http://localhost:8100/api/bins/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                bins: data.bins
            })
        }
    }

    handleModelNameChange = event => {
        const value = event.target.value;
        this.setState({
            modelName: value
        });
    }

    handlePictureUrlChange = event => {
        const value = event.target.value;
        this.setState({
            pictureUrl: value
        })
    }

    handleManufacturerChange = event => {
        const value = event.target.value;
        this.setState({
            manufacturer: value
        })
    }

    handleColorChange = event => {
        const value = event.target.value;
        this.setState({
            color: value
        })
    }

    handleBinChange = event => {
        const value = event.target.value;
        this.setState({
            bin: value
        })
    }

    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.model_name = data.modelName;
        data.picture_url = data.pictureUrl;
        delete data.modelName;
        delete data.pictureUrl;
        delete data.bins;

        const resp = await fetch('http://localhost:8080/api/shoes/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newShoe = await resp.json();
            window.location.href = "/shoes";
            const cleared = {
                modelName: "",
                pictureUrl: "",
                manufacturer: "",
                color: "",
                bins: [],
            };
            this.setState(cleared);
        }
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <br />
                    <Link to="/shoes" className="btn btn-primary btn-lg px-4 gap-3">Back</Link>
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleModelNameChange} value={this.state.modelName} placeholder="Model Name" required type="text" name="modelName" id="modelName" className="form-control"/>
                            <label htmlFor="modelName">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handlePictureUrlChange} value={this.state.pictureUrl} placeholder="Picture URL" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"/>
                            <label htmlFor="pictureUrl">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" id="bin" className="form-select">
                                <option value="">Bin</option>
                                {
                                    this.state.bins.map(bin => {
                                        return <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
                                    })
                                }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoeForm;