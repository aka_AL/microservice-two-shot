import { React, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import ShoeDelete from './ShoeDelete';


function ShoeDetails() {
    const { id } = useParams();

    const [shoe, setShoe] = useState("");

    useEffect(() => {
        const fetchData = async () => {
            try {
                const resp =  await fetch(`http://localhost:8080/api/shoes/${id}/`)
                const data = await resp.json();
                setShoe(data);
            } catch (error) {
                console.log("error: ", error)
            }
        }
        fetchData();
    }, []);

    return (
        <div>
            <br />
            <Link to="/shoes" className="btn btn-primary btn-lg px-4 gap-3">Back</Link>
            <br />
            <br />
            <h1>{shoe.model_name}</h1>
            <h4>{shoe.manufacturer}</h4>
            <img src={shoe.picture_url} width="600"/>
            <br />
            <br />
            <h5>Color: {shoe.color}</h5>
            <p>Description: {shoe.description}</p>
            <br />
            <button onClick={() => ShoeDelete(shoe.id)} className="btn btn-primary btn-lg px-4 gap-3">Delete Shoe</button>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    );

}

export default ShoeDetails;