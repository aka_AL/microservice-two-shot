// import { Link } from "react-router-dom"

import { Link } from "react-router-dom";

async function HatDelete(hat) {
    const HatDeleteURL = `http://localhost:8090/api/hats/${hat.id}`
    const fetchConfig = {
        method: "delete"
    }
    const response = await fetch(HatDeleteURL, fetchConfig);
    if (response.ok) {
        const data = await response.json()
        window.location.reload();
    }
}

function HatsList(props) {
    return (
        <div>
            <table className="table">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Color</th>
                        <th>Style</th>
                        <th>Location</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {/* <tr>
                        <th>placeholder 1</th>
                        <th>placeholder 2</th>
                        <th>placeholder 3</th>
                        <th>placeholder 4</th>
                    </tr> */}
                    {props.hats.map(hat => {
                        return (
                            <tr key={hat.id}>
                                <th><Link to="${hat.id}">{ hat.fabric }</Link></th>
                                <th>{ hat.color }</th>
                                <th>{ hat.style_name }</th>
                                <th>{ hat.locationVO.import_href }</th>
                                <th><button onClick={() => HatDelete(hat)}>Delete</button></th>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <div>
                <a href="hats/new"><button className="btn btn-primary">Create New</button></a>
            </div>
        </div>
        
    )
}

export default HatsList



