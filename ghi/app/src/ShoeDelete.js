const ShoeDelete = async id => {
    const shoeId = id;
    const resp = await fetch(`http://localhost:8080/api/shoes/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (resp.ok) {
        const deleteShoe = await resp.json();
        window.location.href = "/shoes";
    }
}

export default ShoeDelete;