addEventListener('DOMContentLoaded', async() => {
    const url = "http://localhost:8100/api/locations/"

    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            //should give us list of locations
            const selectTag = document.getElementById("locationVO")
            for (let location of data.locations) {
                var option = document.createElement('option')
                option.value = location['id']
                option.innerHTML = location["closet_name"]
                selectTag.appendChild(option)
            }

            const formTag = document.getElementById('create-hat-form');
            formTag.addEventListener('submit', async(event) => {
                event.preventDefault();
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData))

                const hatsUrl = "http://localhost:8090/api/hats/"
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        "Content-Type": "application/json"
                    }
                }
                const response = await fetch(hatsUrl, fetchConfig)
                if (response.ok) {
                    formTag.requestFullscreen()
                    const newHat = await response.json()
                    console.log(newHat)
                } else {
                    console.log("event listener not working")
                }
            })
        }
    } catch (e) {
        console.log("Oops, something went wrong")
    }
})