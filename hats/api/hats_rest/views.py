from django.shortcuts import render
from .models import Hat, LocationVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
# Create your views here.


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "fabric","style_name", "color", "picture_url", "locationVO"]
    encoders = {
        "locationVO": LocationVODetailEncoder(),
    }



class HatDetailListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric","style_name", "color", "picture_url"]
    encoders = {
        "locationVO": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(locationVO=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else: 
        content = json.loads(request.body)
        
        try:
            location_href = content["locationVO"]
            locationVO = LocationVO.objects.get(import_href=location_href)
            content["locationVO"] = locationVO
        
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False,
        )


@require_http_methods(['PUT', 'DELETE', 'GET'])
def api_show_hat(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailListEncoder
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                locationVO = LocationVO.objects.get(import_href=location_href)
                content["location"] = locationVO
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist"}, status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hats = Hat.objects.get(id=pk)
        return JsonResponse(hats, encoder=HatDetailListEncoder, safe=False)
